
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ReadXMLfile {
    private ArrayList<Nod> listOfNodes=new ArrayList<>();
    private ArrayList<Arc> listOfArches=new ArrayList<>();


    public ArrayList<Nod> getListOfNodes() {
        return listOfNodes;
    }

    public ArrayList<Arc> getListOfArches() {
        return listOfArches;
    }

    public ReadXMLfile(){
        try {
            File readMap = new File("src\\LuxemburgMap.xml");
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            UserHandler userhandler = new UserHandler();
            saxParser.parse(readMap, userhandler);

            this.listOfNodes=userhandler.getNodes();
            this.listOfArches=userhandler.getArches();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
class UserHandler extends DefaultHandler {
    private ArrayList<Nod> nodes =new ArrayList<>();
    private ArrayList<Arc> arches =new ArrayList<>();

    public ArrayList<Arc> getArches() {
        return arches;
    }

    public ArrayList<Nod> getNodes() {
        return nodes;
    }

    @Override
    public void startElement(
            String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        if (qName.equalsIgnoreCase("node")) {

            String id = attributes.getValue("id");
            String longitude = attributes.getValue("longitude");
            String latitude = attributes.getValue("latitude");

            Nod n = new Nod(Integer.parseInt(id), Integer.parseInt(latitude), Integer.parseInt(longitude));
            nodes.add(n);
        } else if (qName.equalsIgnoreCase("arc")) {


            String from = attributes.getValue("from");
            String to = attributes.getValue("to");
            String length = attributes.getValue("length");

            Arc arc = new Arc(Integer.parseInt(from), Integer.parseInt(to), Integer.parseInt(length));
            arches.add(arc);
        }

    }
}
