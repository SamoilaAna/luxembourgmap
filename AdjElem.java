
import java.util.ArrayList;

public class AdjElem {

    private ArrayList<Arc> neighbors;

    public AdjElem(Nod node, ArrayList<Arc> neighbors){

        this.neighbors=neighbors;
    }
    
    public AdjElem(){
        this.neighbors=new ArrayList<>();
    }
    
    public void addElemement(Arc arc){
        this.neighbors.add(arc);
    }
    
    public ArrayList<Arc> getNeighbors() {
        return neighbors;
    }
    
	public void setNeighbors(ArrayList<Arc> neighbors) {
		this.neighbors = neighbors;
	}
}
