
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

public class Panel extends JPanel {
    private ArrayList<Nod> nodes;
    private ArrayList<Arc> arches;
    private ArrayList<Arc> path;
    private Map<Integer, AdjElem> list;

    Nod minLatitude;
    Nod maxLatitude;
    Nod maxLongitude;
    Nod minLongitude;
    
    JButton DijkstraButton =new JButton("Choose Dijkstra");
    boolean dijkstra=false;
    JButton BellmanFordButton =new JButton("Choose BellmanFord");
    boolean bellmanFord=false;
    int count=0;
    Nod pointStart = new Nod();
    Nod pointEnd = new Nod();
    public Panel() {
        ReadXMLfile read = new ReadXMLfile();
        list = new HashMap<>();
        nodes = new ArrayList<>();
        nodes = read.getListOfNodes();
        arches = new ArrayList<>();
        arches = read.getListOfArches();
        path = new ArrayList<>();
        minLatitude = Collections.min(nodes, Comparator.comparing(Nod::getLatitude));
        maxLatitude = Collections.max(nodes, Comparator.comparing(Nod::getLatitude));
        minLongitude = Collections.min(nodes, Comparator.comparing(Nod::getLongitude));
        maxLongitude = Collections.max(nodes, Comparator.comparing(Nod::getLongitude));
        initialize();
        setBorder(BorderFactory.createLineBorder(Color.black));
        add(DijkstraButton, BorderLayout.LINE_END);
        add(BellmanFordButton, BorderLayout.LINE_END);

        BellmanFordButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                path = null;
                repaint();
                bellmanFord = true;
                count = 0;
                pointStart = new Nod();
                pointEnd = new Nod();
                dijkstra = false;

            }
        });
        DijkstraButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                path=null;
                repaint();
                dijkstra = true;
                count = 0;
                pointStart = new Nod();
                pointEnd = new Nod();
                bellmanFord = false;
                repaint();
            }
        });

        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (dijkstra || bellmanFord) {
                    count++;
                    if (count < 2) {

                        setLatitude(pointStart, e.getX());
                        setLongitude(pointStart, e.getY());
                        pointStart = FindClosestNode(pointStart);
                        repaint();
                    } else if (count == 2) {
                        setLatitude(pointEnd, e.getX());
                        setLongitude(pointEnd, e.getY());
                        pointEnd = FindClosestNode(pointEnd);
                        repaint();
                        initialize();
                        if (dijkstra) {
                        	System.out.println("Time for Dijkstra:");
                            System.out.println("     Start: "+java.time.LocalTime.now());
                            path = FindPath(Dijkstra(pointStart.getId(), pointEnd.getId()));
                            System.out.println("     End: "+java.time.LocalTime.now());
                            System.out.println("-------------------------------------------");
                            repaint();
                        } else if (bellmanFord) {
                        	System.out.println("Time for Bellman Ford:");
                            System.out.println("     Start: "+java.time.LocalTime.now());
                            path = FindPath(BellmanFord(pointStart.getId()));
                            System.out.println("     End: "+java.time.LocalTime.now());
                            System.out.println("-------------------------------------------");
                            repaint();
                        }
                    }
                }
            }
        });
    }

    ArrayList<Arc> FindPath(ArrayList<Integer> algorithmOutput){
        int index=pointEnd.getId();
        ArrayList<Arc> path=new ArrayList<>();
        do{
            int aux=algorithmOutput.get(index);
            Arc arc=new Arc(aux, index);
            path.add(arc);
            index=aux;
        }while(index!= pointStart.getId());

        return path;
    }

    void initialize(){
        for(Nod node: nodes){
            list.put(node.getId(), new AdjElem());
        }
        for(Arc arc: arches){
            list.get(arc.getFrom()).addElemement(arc);
            list.replace(arc.getFrom(), list.get(arc.getFrom()));
        }


    }

    Nod FindClosestNode(Nod node){
        ArrayList<Nod> closest=new ArrayList<>();
        Nod nod=new Nod();
        int minim=Integer.MAX_VALUE;
        for(Nod n: nodes){
            if(node.getLongitude()==n.getLongitude() && node.getLatitude()==n.getLatitude()){
                return n;
            }
            if(n.CheckCollision(node.getLatitude(), node.getLongitude())!=-1.00f){

                if((float)minim>n.CheckCollision(node.getLatitude(), node.getLongitude()))
                {
                    closest.add(n);
                    minim=(int)(n.CheckCollision(node.getLatitude(), node.getLongitude()));
                }

            }
        }
        if(closest.size()!=0){
            return closest.get(closest.size()-1);
        }
        else{
            return nod;
        }
    }
  
    public ArrayList<Integer> BellmanFord(int nodeStart) {
        ArrayList<Integer> distance = new ArrayList<>(Collections.nCopies(nodes.size(), Integer.MAX_VALUE));
        ArrayList<Integer> parents = new ArrayList<>(Collections.nCopies(nodes.size(), -1));
        distance.set(nodeStart, 0);
        boolean change;
        for (int node = 0; node < nodes.size() - 1; node++) {
            change = false;
            for(int arc = 0; arc< arches.size(); arc++){
                int from = arches.get(arc).getFrom();
                int to = arches.get(arc).getTo();
                int length = arches.get(arc).getLength();
                if (distance.get(from) != Integer.MAX_VALUE && distance.get(from) + length < distance.get(to)) {
                    parents.set(to, from);
                    distance.set(to, distance.get(from) + length);
                    change = true;
                }

            }
            if (!change) {
                return parents;
            }
        }
        return null;
    }

    public ArrayList<Integer> Dijkstra(int nodeStart, int nodeEnd) {
        ArrayList<Integer> dist = new ArrayList<>(Collections.nCopies(nodes.size(), Integer.MAX_VALUE));
        ArrayList<Integer> parents = new ArrayList<>(Collections.nCopies(nodes.size(), -1));
        ArrayList<Boolean> visited = new ArrayList<>(Collections.nCopies(nodes.size(), false));
        PriorityQueue<Arc> priorityQueue=new PriorityQueue<>(Comparator.comparingInt(Arc::getLength));
  
        dist.set(nodeStart, 0);
        priorityQueue.add(new Arc(nodeStart,nodeStart,0));
        while(!priorityQueue.isEmpty()){
            int currentNode=priorityQueue.peek().getTo();

            if (visited.get(currentNode).equals(false)) {

                visited.set(currentNode, true);

                if (currentNode == nodeEnd) {
                    return parents;
                }

                ArrayList<Arc> neighbors = list.get(currentNode).getNeighbors();
                for (Arc neighbor : neighbors) {

                    int distance = neighbor.getLength();
                    int neigborDestination = neighbor.getTo();

                    if (visited.get(neigborDestination).equals(false)
                            && dist.get(currentNode) + distance < dist.get(neigborDestination)) {

                        dist.set(neigborDestination, dist.get(currentNode) + distance);
                        parents.set(neigborDestination, currentNode);
                        
                        Arc a = new Arc(currentNode, neigborDestination, dist.get(neigborDestination));
                        priorityQueue.add(a);

                    }
                }
            }
            priorityQueue.poll();


        }
        return null;
    }

   

    protected void paintComponent(Graphics graph) {
        super.paintComponent(graph);
        this.setBackground(Color.PINK);
        for (Arc arc : arches) {
            arc.DrawArc(graph, nodes,
                    minLatitude.getLatitude(),
                    maxLongitude.getLongitude(),
                    maxLatitude.getLatitude(),
                    minLongitude.getLongitude(), getScale(), Color.BLACK);
        }

        if(dijkstra && path!=null){
            for (Arc arc : path) {

                double latitude = nodes.get(arc.getFrom()).getLatitude();
                latitude = nodes.get(arc.getFrom()).TransformLatitude((int) latitude,
                        minLatitude.getLatitude(), maxLatitude.getLatitude(),
                        getScale());
                double longitudine = nodes.get(arc.getFrom()).getLongitude();
                longitudine = nodes.get(arc.getFrom()).TransformLongitude((int) longitudine,
                        minLongitude.getLongitude(),
                        maxLongitude.getLongitude(), getScale());
                Nod node = new Nod();
                node.DrawRoad(graph, latitude, longitudine);
            }
        }
        else {
            if(bellmanFord && path!=null){
                for (Arc arc : path) {
                    double latitudine = nodes.get(arc.getFrom()).getLatitude();
                    latitudine = nodes.get(arc.getFrom()).TransformLatitude((int) latitudine, minLatitude.getLatitude(),
                            maxLatitude.getLatitude(), getScale());
                    double longitudine = nodes.get(arc.getFrom()).getLongitude();
                    longitudine = nodes.get(arc.getFrom()).TransformLongitude((int) longitudine, minLongitude.getLongitude(),
                            maxLongitude.getLongitude(), getScale());
                    Nod node = new Nod();
                    node.DrawRoad(graph, latitudine, longitudine);
                }
            }
        }
        if(dijkstra||bellmanFord){
            if(!pointStart.equals(new Nod())) {
                pointStart.DrawNode(graph, pointStart.TransformLatitude(pointStart.getLatitude(), minLatitude.getLatitude(),
                        maxLatitude.getLatitude(), getScale()),
                        pointStart.TransformLongitude(pointStart.getLongitude(), minLongitude.getLongitude(), maxLongitude.getLongitude(),
                                getScale()));
            }
            if(!pointEnd.equals(new Nod())) {
                pointEnd.DrawNode(graph, pointEnd.TransformLatitude(pointEnd.getLatitude(), minLatitude.getLatitude(),
                        maxLatitude.getLatitude(), getScale()),
                        pointEnd.TransformLongitude(pointEnd.getLongitude(), minLongitude.getLongitude(), maxLongitude.getLongitude(),
                                getScale()));
            }
        }
    }
    private void setLatitude(Nod n, int coord) {
        n.setLatitude((int) (coord * ((maxLatitude.getLatitude() - minLatitude.getLatitude()) / getScale()) + minLatitude.getLatitude()));
    }

    private void setLongitude(Nod n, int coord) {
        n.setLongitude((int) (coord * ((maxLongitude.getLongitude() - minLongitude.getLongitude()) / getScale()) + minLongitude.getLongitude()));
    }
    private double getScale(){
        return Math.min(this.getWidth(), this.getHeight())-50.00f;
    }
}
