
import java.awt.*;

public class Nod {
    private int id;
    private int longitude;
    private int  latitude;

    public Nod(int id, int latitude, int longitude){
        this.id=id;
        this.latitude=latitude;
        this.longitude=longitude;
    }
    public Nod(int id){
        this.id=id;
    }
    public Nod(){
        this.id=0;
        this.latitude=0;
        this.longitude=0;
    }
    public Nod(int latitude, int longitude){
        this.latitude=latitude;
        this.longitude=longitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public int getLatitude() {
        return latitude;
    }

    public int getLongitude() {
        return longitude;
    }
    public double TransformLatitude(int lat, int minLat, int maxLat, double scale){
        return (lat-minLat) /((maxLat-minLat)/ scale);
    }
    public double TransformLongitude(int longt, int minLong, int maxLong, double scale){
        return (longt-minLong)/((maxLong -minLong)/ scale);
    }
    public void DrawRoad(Graphics g, double coordX, double coordY){
        g.setColor(Color.MAGENTA);
        g.fillOval((int)coordX, (int)coordY, 2, 2);
        g.setColor(Color.MAGENTA);
        g.drawOval((int)coordX, (int)coordY, 2, 2);

    }
    public void DrawNode(Graphics g, double coordX, double coordY)
    {

        g.setColor(Color.CYAN);

        g.fillOval((int)coordX, (int)coordY, 10, 10);
        g.setColor(Color.BLACK);
        g.drawOval((int)coordX, (int)coordY, 10, 10);

    }

    public float CheckCollision(int coordX, int coordY) {
        float firstSide = Math.abs(this.latitude - coordX);
        float secondSide= Math.abs(this.longitude - coordY);
        firstSide = firstSide * firstSide;
        secondSide = secondSide * secondSide;
        float distance = (float) Math.sqrt(firstSide+secondSide);
        if(distance < 1000.0f){
            return distance;
        }
        return -1.0f;
    }


}
